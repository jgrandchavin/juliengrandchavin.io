import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { HomeComponent } from './components/home/home.component';
import { AppComponent } from './app.component';
import { SkillsComponent } from './components/skills/skills.component';
import { LabComponent } from './components/lab/lab.component';
import { ContactComponent  } from './components/contact/contact.component';
import { AboutMeComponent } from './components/aboutMe/aboutMe.component';

const routes: Routes = [
  { path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { animation: 'home' }
  },
  {
    path: 'about-me',
    component: AboutMeComponent,
    data: { animation : 'about-me'}
  },
  {
    path: 'skills',
    component: SkillsComponent,
    data: { animation: 'skills' }
  },
  {
    path: 'lab',
    component: LabComponent,
    data: { animation: 'lab' }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { animation: 'contact' }
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SkillsComponent,
    LabComponent,
    ContactComponent,
    AboutMeComponent
  ],
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: true, useHash: true }// <-- debugging purposes only
    ),
    BrowserModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
