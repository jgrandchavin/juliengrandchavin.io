import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';

// Animations
import { routerAnimation } from './animations/router-animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [routerAnimation]
})

export class AppComponent {

  homeLogo = true;
  skillsLogo = true;
  labLogo = true;
  contactLogo = true;
  aboutMeLogo = true;

  location: Location;

  constructor( location: Location, public router: Router) {
    this.location = location;
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  /**
   * Use for router animation.
   *
   * @param outlet current outlet.
   * @returns {any} animation.
   */
  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.animation ;
  }
}
